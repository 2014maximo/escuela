import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ContactosModel } from '../Models/contactos.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactosService {

  private url = 'http://localhost:3000/contactos';

  constructor(private http: HttpClient) { }


  cargarContactos(): Observable<ContactosModel[]> {
    return this.http.get<ContactosModel[]>(this.url);
  }

  consultarContactos(idContactos: string): Observable<ContactosModel> {
    return this.http.get<ContactosModel>(this.url + idContactos);
  }

  guardar(datosConstactos: ContactosModel): Observable<ContactosModel> {
    return this.http.post<ContactosModel>(this.url, datosConstactos);
  }

  actualizar(datosContactos: ContactosModel): Observable<ContactosModel> {
    return this.http.put<ContactosModel>(this.url + datosContactos.id, datosContactos);
  }

  eliminar(idEliminar: string): Observable<ContactosModel> {
    return this.http.delete<ContactosModel>(`${this.url}${idEliminar}`);
  }
}
