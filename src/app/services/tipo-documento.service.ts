import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TipoDocumentoModel } from '../Models/tipoDocumento.model';
import { SelectItem } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class TipoDocumentoService {

  private url = 'http://localhost:3000/tipoDocumento';

  constructor(private http: HttpClient) { }

  consultarTipoDocumento(): Observable<SelectItem[]> {
    return this.http.get<SelectItem[]>(this.url);
  }

  consultar(idTipoDocumento: string): Observable<TipoDocumentoModel> {
    return this.http.get<TipoDocumentoModel>(this.url + idTipoDocumento);
  }

  guardar(datosDocumentos: TipoDocumentoModel): Observable<TipoDocumentoModel> {
    return this.http.post<TipoDocumentoModel>(this.url, datosDocumentos);
  }

  actualizar(datosDocumentos: TipoDocumentoModel): Observable<TipoDocumentoModel> {
    return this.http.put<TipoDocumentoModel>(this.url + datosDocumentos.idTipoDocumento, datosDocumentos);
  }

  eliminar(idEliminar: string): Observable<TipoDocumentoModel> {
    return this.http.delete<TipoDocumentoModel>(`${this.url}${idEliminar}`);
  }


}
