import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UsuariosModel } from '../Models/usuarios.model';


@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  private url = 'http://localhost:3000/informacion-basica/';

  constructor( private http: HttpClient ) { }

  consultarUsuarios(): Observable<UsuariosModel[]> {
    return this.http.get<UsuariosModel[]>(this.url);
  }

  consultar(idUsuarios: string): Observable<UsuariosModel> {
    return this.http.get<UsuariosModel>(this.url + idUsuarios);
  }

  guardar(datosUsuarios: UsuariosModel): Observable<UsuariosModel> {
    return this.http.post<UsuariosModel>(this.url, datosUsuarios); 
  }

  actualizar(datosUsuarios: UsuariosModel): Observable<UsuariosModel> {
    return this.http.put<UsuariosModel>(this.url + datosUsuarios.id, datosUsuarios);
  }

  eliminar(idEliminar: string): Observable<UsuariosModel>{
    return this.http.delete<UsuariosModel>(`${this.url}${idEliminar}`);
  }
}
