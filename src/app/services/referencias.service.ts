import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ReferenciaModel } from '../Models/referencia.model';

@Injectable({
  providedIn: 'root'
})
export class ReferenciasService {

  private url = 'http://localhost:3000/referencias/';

  constructor( private http: HttpClient) { }

  cargarReferencia(): Observable <ReferenciaModel[]> {
    return this.http.get<ReferenciaModel[]>(this.url);
  }

  consultarReferencias(idRefencia: string): Observable<ReferenciaModel> {
    return this.http.get<ReferenciaModel>(this.url + idRefencia);
  }

  guardar(datosReferencia: ReferenciaModel): Observable<ReferenciaModel> {
    return this.http.post<ReferenciaModel>(this.url, datosReferencia);
  }

  actualizar( datosReferencia: ReferenciaModel ): Observable<ReferenciaModel> {
    return this.http.put<ReferenciaModel>(this.url + datosReferencia.id, datosReferencia);
  }

  eliminar( idEliminar: string ): Observable<ReferenciaModel> {
    return this.http.delete<ReferenciaModel>(`${this.url}${idEliminar}`);
  }

}
