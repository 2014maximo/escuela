import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SelectItem } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class UbicacionService {

  private url = 'http://localhost:3000/ubicacion/';

  constructor( private http: HttpClient ) { }

  consultarCiudad(): Observable<SelectItem[]> {
    return this.http.get<SelectItem[]>(this.url);
  }
}
