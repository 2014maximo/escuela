import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContactosService } from '../../services/contactos.service';
import { ConfirmationService, SelectItem } from 'primeng/api';
import { ActivatedRoute } from '@angular/router';
import { DireccionModel } from '../../Models/direccion.model';
import { of, observable } from 'rxjs';

@Component({
  selector: 'app-direccion',
  templateUrl: './direccion.component.html',
  styleUrls: ['./direccion.component.scss']
})
export class DireccionComponent implements OnInit {

  @Input() residencia = true;
  @Input() direccionIN = new DireccionModel();
  @Output() direccionOut = new EventEmitter<DireccionModel>();

  form: FormGroup;
  tiposVia: SelectItem[];
  msgs = [];
  public direccionCompleta: string [];

  constructor(private fb: FormBuilder,
    private contactosService: ContactosService,
    private confirmationService: ConfirmationService,
    private router: ActivatedRoute) {

    this.form = this.fb.group({
      id: [null],
      tipo: [''],
      via: ['',
      [
        Validators.required
      ]],
      numero1: [null,
        [
          Validators.required,
          Validators.pattern(/^([0-9])*$/) // solo numeros
        ]],
      letra1: ['',
      [
        Validators.pattern(/^[A-Za-z\s\xF1\xD1]+$/) // solo letras
      ]],
      numero2: [null,
        [
          Validators.required,
          Validators.pattern(/^([0-9])*$/) // solo numeros
        ]],
      letra2: ['',
      [
        Validators.pattern(/^[A-Za-z\s\xF1\xD1]+$/) // solo letras
      ]],
    });
  }

  ngOnInit() {

    

    this.tiposVia = [
      { label: 'Seleccione', value: null },
      { label: 'Calle', value: '1' },
      { label: 'Carrera', value: '2' },
      { label: 'Diagonal', value: '3' },
      { label: 'Circular', value: '4' }
    ];
    
    const obs$ = of(this.tiposVia);
    
    obs$.subscribe(
        next => console.log('next', next.values),
        null,
        () => console.log('Termina')
    );

    if (this.direccionIN) {
      this.form.patchValue(this.direccionIN);
    }

  } 

  impresionDireccion() {
    this.direccionCompleta = this.form.get('via').value + // TODO esta llegando indefinido
    this.form.get('numero1').value + this.form.get('letra1').value + this.form.get('numero2').value + this.form.get('letra2').value;
  }
  continuar() {

    if (!this.form.invalid) {

      const dirOut = new DireccionModel();
      dirOut.id = this.form.get('id').value;
      dirOut.numero1 = this.form.get('numero1').value;
      dirOut.numero2 = this.form.get('numero2').value;
      dirOut.letra1 = this.form.get('letra1').value;
      dirOut.letra2 = this.form.get('letra2').value;
      dirOut.via = this.form.get('via').value;

      // formar direccion
      dirOut.formato = dirOut.via + dirOut.numero1 + dirOut.letra1 +
        ' - #' + dirOut.numero2 + dirOut.letra2;

      if (this.residencia) {
        dirOut.tipo = 1;
      } else {
        dirOut.tipo = 2;
      }

      if (this.direccionIN) {
        dirOut.personaID = this.direccionIN.personaID;
      }

      this.direccionOut.emit(dirOut);

    }

  }

}
