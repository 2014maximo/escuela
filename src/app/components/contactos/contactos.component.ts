import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Message } from 'primeng/components/common/message';
import { ContactosModel } from '../../Models/contactos.model';
import { ContactosService } from '../../services/contactos.service';
import { ConfirmationService } from 'primeng/api';
import { ActivatedRoute, Router } from '@angular/router';
import { DireccionModel } from '../../Models/direccion.model';
import { UsuariosService } from '../../services/usuarios.service';

@Component({
  selector: 'app-contactos',
  templateUrl: './contactos.component.html',
  styles: []
})
export class ContactosComponent implements OnInit {

  @Input() personaID = true;

  msgs: Message[] = [];
  formContacto: FormGroup;
  contacto: ContactosModel;
  contactos: ContactosModel[];
  contactoActual: ContactosModel;
  mostrarDirResidencia = false;
  direccionResidencia = new DireccionModel();
  idContactos: string;

  constructor(private contactosService: ContactosService,
    private fb: FormBuilder,
    private routerNav: Router,
    private confirmationService: ConfirmationService,
    private router: ActivatedRoute) {

  }

  ngOnInit() {

    this.idContactos = this.router.snapshot.paramMap.get('id');

    this.formContacto = this.fb.group({
      id: [null],
      direccionResidencia: ['',
        [
          Validators.required
        ]],
      telefono: ['',
        [
          Validators.required,
          Validators.pattern(/^([0-9])*$/) // solo numeros
        ]]
    });
    this.consultarElementos();

    this.direccionResidencia.id = 1;
    this.direccionResidencia.numero1 = "";
    this.direccionResidencia.numero2 = "";
    this.direccionResidencia.letra1 = "";
    this.direccionResidencia.letra2 = "";
    this.direccionResidencia.via = "";

  }

  recibirDirResidencia(direccion: DireccionModel) {
    this.direccionResidencia = direccion;
    this.mostrarDirResidencia = false;
    this.direccionResidencia.personaID = 1;
    this.formContacto.get('direccionResidencia').setValue(direccion.formato);


  }

  showDirResidencia() {
    this.mostrarDirResidencia = true;
  }

  consultarElementos() {
    this.contactosService.cargarContactos().subscribe(datos => {
      this.contactos = datos;
    });
  }

  guardarContacto() {
    let mensaje = '';

    if (!this.contacto) {
      this.contacto = new ContactosModel();
    }

    this.contacto.direccionResidencia = this.formContacto.get('direccionResidencia').value;
    this.contacto.telefono = this.formContacto.get('telefono').value;

    if (this.idContactos) {

      this.contactosService.actualizar(this.contacto).subscribe(data => {

        this.formContacto.reset();
        mensaje = 'Registro actualizado con éxito.';
      });
    } else {
      this.contactosService.guardar(this.contacto).subscribe(data => {

        this.formContacto.reset();
        mensaje = 'Registro guardado con éxito';


      });
    }




    /* this.msgs = [];
    this.msgs.push({
      severity: 'success',
      summary: 'Informacion',
      detail: 'Guardado satisfactioriamente.'
    }); */


  }/* end guardar(); */

} /* end class ContactosComponent */
