import { Component, OnInit } from '@angular/core';
import { UsuariosModel } from 'src/app/Models/usuarios.model';
import { Message, ConfirmationService } from 'primeng/api';
import { UsuariosService } from '../../services/usuarios.service';
import { ActivatedRoute, Routes } from '@angular/router';

@Component({
  selector: 'app-listar-usuarios',
  templateUrl: './listar-usuarios.component.html',
  styles: []
})
export class ListarUsuariosComponent implements OnInit {

  usuarios: UsuariosModel[] = [];
  msgs: Message[] = [];

  constructor( private usuariosService: UsuariosService,
               private confirmationService: ConfirmationService,
               private router: ActivatedRoute ) {}

               ngOnInit() {
                this.consultarElementos();

                const mensaje = this.router.snapshot.paramMap.get('mensaje');
                this.msgs = [];
                if (mensaje) {
                  this.msgs.push({
                    severity: 'success',
                    summary: 'Informacion',
                    detail: mensaje
                  });
                }
              }

              consultarElementos() {
                this.usuariosService.consultarUsuarios().subscribe(datos => {
                  this.usuarios = datos;
                });
              }

              confirmarEliminar(idEliminar: string, idnombre: string) {
                this.confirmationService.confirm({
                  message: `Esta seguro de eliminar a ${idnombre}?`,
                  accept: () => {

                    this.usuariosService.eliminar(idEliminar).subscribe(data => {
                      this.msgs = [];
                      this.msgs.push({
                        severity: 'success',
                        summary: 'Informacion',
                        detail: 'Registro eliminado con exito.'
                      });

                      this.consultarElementos();

                    });
                  }
                });
              }

            }
