import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormsModule } from '@angular/forms';
import { TipoDocumentoService } from '../../services/tipo-documento.service';
import { TipoDocumentoModel } from '../../Models/tipoDocumento.model';
import { Message } from 'primeng/components/common/message';
import { Router, ActivatedRoute } from '@angular/router';
import { UsuariosModel } from 'src/app/Models/usuarios.model';
import { UsuariosService } from '../../services/usuarios.service';
import { SelectItem } from 'primeng/api';
import { INFO_BASICA } from '../../constants/generales.contant';
import { UbicacionService } from '../../services/ubicacion.service';
import * as moment from 'moment';
@Component({
  selector: 'app-informacion-basica',
  templateUrl: './informacion-basica.component.html',
  styles: []
})
export class InformacionBasicaComponent implements OnInit {

  @Input() personaID: string;

  public form: FormGroup;
  public tipoDocumento: TipoDocumentoModel[] = [];
  public tipoModel: TipoDocumentoModel;
  public usuario: UsuariosModel;
  public msgs: Message[] = [];
  public idUsuario: string;
  public hoy: string;
  public es = INFO_BASICA.es;
  public desactivarAgregar = false;
  public minFecha: Date;
  public maxFecha: Date;
  public validarFecha = false;


  public tDocumento: SelectItem[];
  public ciudad: SelectItem[];

  constructor(private tipoDocumentoService: TipoDocumentoService,
    private fb: FormBuilder,
    private usuarioService: UsuariosService,
    private ubicacionService: UbicacionService,
    private router: ActivatedRoute,
    private routerNav: Router) {


    /* FORMULARIO ÚNICO */
    this.form = this.fb.group({
      id: [null],
      documento: ['',
        [
          Validators.required,
          Validators.pattern(/^([0-9])*$/) // solo numeros
        ]],
      nombre: ['',
        [
          Validators.required,
          Validators.pattern(/^[A-Za-z\s\xF1\xD1]+$/), // solo letras
          Validators.maxLength(50),
          Validators.minLength(3)
        ]],
      apellido: ['',
        [
          Validators.required,
          Validators.maxLength(50),
          Validators.minLength(3)
        ]],
      tipo_documento: ['',
      [
        Validators.required
      ]
    ],
      lugar_nacimiento: ['',
      [
        Validators.required
      ]],
      fecha_nacimiento: ['',
        [
          Validators.required
        ]
      ],
      edad: ['',
      [
        Validators.required
      ]]
    });

  }

  ngOnInit() {

    this.cargarCiudad();
    this.cargarTipoDocumento();
    this.cargaEdad();

    this.idUsuario = this.router.snapshot.paramMap.get('id');
    if (this.idUsuario) {
      this.usuarioService.consultar(this.idUsuario).subscribe(data => {
        this.usuario = data;
        this.form.get('id').setValue(this.usuario.id);
        this.form.get('documento').setValue(this.usuario.documento);
        this.form.get('tipo_documento').setValue(this.usuario.tipo_documento);
        this.form.get('nombre').setValue(this.usuario.nombre);
        this.form.get('apellido').setValue(this.usuario.apellido);
        const formato = new Date(this.usuario.fecha_nacimiento);
        this.form.get('fecha_nacimiento').setValue(formato);
        this.form.get('lugar_nacimiento').setValue(this.usuario.lugar_nacimiento);
        this.form.get('edad').setValue(this.usuario.edad);

      });
    }
  }

  cargarCiudad() {
    this.ubicacionService.consultarCiudad().subscribe(datos => {
      this.ciudad = datos;
    });
  }

  cargarTipoDocumento() {
    this.tipoDocumentoService.consultarTipoDocumento().subscribe(datos => {
      this.tDocumento = datos;
    });
  }

  cargaEdad() {
  }

  calcularEdad() {
    if (this.form.get('fecha_nacimiento').value) {
      const fnacimiento = this.form.get('fecha_nacimiento').value;
      const today = new Date();
      const age: number = today.getFullYear() - fnacimiento.getFullYear();

      if (age >= 18) {
        this.minFecha = fnacimiento;
        const formato = new Date(moment(today).format('YYYY-MM-DD'));
        this.maxFecha = new Date(formato);
        this.form.get('edad').setValue(age);
        this.validarFecha = true;
      } else {
        this.form.get('edad').setValue('');

        this.msgs.push({
          severity: 'success',
          summary: 'Informacion',
          detail: 'Debe ser mayor de edad.'
        });

      }
    } else {
      this.msgs.push({
        severity: 'success',
        summary: 'Informacion',
        detail: 'Segundo mensaje de prueba.'
      });

    }
  }
  guardar() {

    console.log(this.form.value);
    const mensaje = '';
    let usuarioNuevo = new UsuariosModel();
    if (this.form.invalid) {
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: 'Error', detail: 'Debes completar la información del formulario' });

    } else {

      if (!this.usuario) {
        this.usuario = new UsuariosModel();
      }

      /* SE PASAN LOS DATOS DEL FORMULARIO AL OBJETO QUE SE VA COMO PARAMETRO AL SERVICIO */
      this.usuario.id = this.form.get('id').value;
      this.usuario.tipo_documento = this.form.get('tipo_documento').value;
      this.usuario.documento = this.form.get('documento').value;
      this.usuario.nombre = this.form.get('nombre').value;
      this.usuario.apellido = this.form.get('apellido').value;
      this.usuario.lugar_nacimiento = this.form.get('lugar_nacimiento').value;
      this.usuario.fecha_nacimiento = this.form.get('fecha_nacimiento').value;
      this.usuario.edad = this.form.get('edad').value;

      if (this.idUsuario) {

        this.usuarioService.actualizar(this.usuario).subscribe(data => {

          this.msgs = [];
          this.msgs.push({
            severity: 'success',
            summary: 'Informacion',
            detail: 'Registro actualizado con éxito.'
          });
        }
        );
      } else {
        usuarioNuevo = this.usuario;
        this.usuarioService.guardar(usuarioNuevo).subscribe(data => {
          this.form.reset();
          this.msgs = [];
          this.msgs.push({
            severity: 'success',
            summary: 'Informacion',
            detail: 'Registro almacenado con éxito.'
          }); /* fin push */
        }); /* fin subscribe */

      }/* fin else */

    }/* fin método guardar */

  } /* fin clase */
}