import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Message } from 'primeng/components/common/message';
import { ReferenciaModel } from '../../Models/referencia.model';
import { ReferenciasService } from '../../services/referencias.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-referencia',
  templateUrl: './referencia.component.html',
  styles: []
})
export class ReferenciaComponent implements OnInit {

  public formReferencia: FormGroup;
  public msgs: Message[] = [];
  public referencia: ReferenciaModel[] = [];


  constructor(private refereniciasService: ReferenciasService,
              private router: ActivatedRoute) {

  }

  ngOnInit() {
    this.cargaReferencia();

    const mensaje = this.router.snapshot.paramMap.get('mensaje');
    this.msgs = [];
    if (mensaje) {
      this.msgs.push({
        severity: 'success',
        summary: 'Informacion',
        detail: mensaje
      });
    }
  }

  cargaReferencia() {
    this.refereniciasService.cargarReferencia().subscribe(datos => {
      this.referencia = datos;
    });
  }
}
