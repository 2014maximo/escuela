export class UsuariosModel {

    public id: number;
    public documento: string;
    public nombre: string;
    public apellido: string;
    public tipo_documento: string;
    public lugar_nacimiento: string;
    public fecha_nacimiento: string;
    public edad: string;

}