export class DireccionModel {

    public id: number;
    public via: string;
    public numero1: string;
    public letra1: string;
    public numero2: string;
    public letra2: string;
    public tipo: number; // 1: residencia, 2 adicional
    public personaID: number;
    public formato: string;

}