import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// NGPRIME
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TableModule } from 'primeng/table';
import { GalleriaModule } from 'primeng/galleria';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { TabViewModule } from 'primeng/tabview';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { CardModule } from 'primeng/card';


// RUTAS
import { ROUTES } from './app.routes';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

// FORMULARIOS
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// COMPONENTES
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { CrearUsuariosComponent } from './components/crear-usuarios/crear-usuarios.component';
import { ListarUsuariosComponent } from './components/listar-usuarios/listar-usuarios.component';
import { from } from 'rxjs';
import { InformacionBasicaComponent } from './components/informacion-basica/informacion-basica.component';
import { ContactosComponent } from './components/contactos/contactos.component';
import { ReferenciaComponent } from './components/referencia/referencia.component';
import { DireccionComponent } from './components/direccion/direccion.component';
import {DialogModule} from 'primeng/dialog';
import {CalendarModule} from 'primeng/calendar';
import { FinalizarComponent } from './components/finalizar/finalizar.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CrearUsuariosComponent,
    ListarUsuariosComponent,
    InformacionBasicaComponent,
    ContactosComponent,
    ReferenciaComponent,
    DireccionComponent,
    FinalizarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    TableModule,
    GalleriaModule,
    MessagesModule,
    CardModule,
    MessageModule,
    ConfirmDialogModule,
    HttpClientModule,
    FormsModule,
    TabViewModule,
    ReactiveFormsModule,
    InputTextModule,
    DropdownModule,
    CalendarModule,
    DialogModule,
    RouterModule.forRoot (ROUTES)
  ],
  providers: [
    ConfirmationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
