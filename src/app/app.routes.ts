import { Routes } from '@angular/router';
import { CrearUsuariosComponent } from './components/crear-usuarios/crear-usuarios.component';
import { ListarUsuariosComponent } from './components/listar-usuarios/listar-usuarios.component';
import { InformacionBasicaComponent } from './components/informacion-basica/informacion-basica.component';
import { ReferenciaComponent } from './components/referencia/referencia.component';
import { ContactosComponent } from './components/contactos/contactos.component';

export const ROUTES: Routes = [

    { path: 'crear', component: CrearUsuariosComponent },
    { path: 'crear/:id', component: CrearUsuariosComponent },
    { path: 'crear/:mensaje', component: CrearUsuariosComponent },
    { path: 'listar', component: ListarUsuariosComponent },
    { path: 'listar/:mensaje', component: ListarUsuariosComponent },
    { path: 'informacion-basica', component: InformacionBasicaComponent },
    { path: 'informacion-basica/:id', component: InformacionBasicaComponent },
    { path: 'contactos', component: ContactosComponent },
    { path: 'contactos/:id', component: ContactosComponent },
    { path: 'contactos/:mensaje', component: ContactosComponent },
    { path: 'referencias', component: ReferenciaComponent },
    { path: 'referencias/:mensaje', component: ReferenciaComponent }

];
